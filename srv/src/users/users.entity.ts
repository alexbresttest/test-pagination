import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from "@nestjs/swagger";

@Entity({ name: 'users' })
export class UsersEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn({ type: 'bigint', primaryKeyConstraintName: 'pk_users_id' })
  id: number;

  @ApiProperty()
  @Column({ name: 'firstname', type: 'varchar', length: 255, nullable: true, default: null })
  firstname: string | null;

  @ApiProperty()
  @Column({ name: 'lastname', type: 'varchar', length: 255, nullable: true, default: null })
  lastname: string | null;

  @ApiProperty()
  @Column({ name: 'phone', type: 'varchar', length: 16, nullable: true, default: null, unique: true })
  phone: string | null;

  @ApiProperty()
  @Column({ name: 'email', type: 'varchar', length: 320, unique: true })
  email: string;

  @ApiProperty()
  @Column({ name: 'created_at', type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @ApiProperty()
  @Column({ name: 'updated_at', type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updatedAt: Date;
}
