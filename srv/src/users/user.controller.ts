import { UserService } from './users.service';
import { Controller, Get, Logger, Query } from '@nestjs/common';
import { UsersResponseDto } from './users.response.dto';
import { ApiPaginatedResponse } from '../decorators/apiPaginatedResponse.decorator';
import { UsersEntity } from './users.entity';
import { ApiExtraModels, ApiQuery } from '@nestjs/swagger';

@Controller('users')
export class UserController {
  private readonly logger = new Logger(UserController.name);

  constructor(private userService: UserService) {}

  @Get()
  @ApiExtraModels(ApiPaginatedResponse)
  @ApiPaginatedResponse(UsersEntity)
  @ApiQuery({ name: 'offset', required: false, type: Number, description: 'default 0' })
  @ApiQuery({ name: 'limit', required: false, type: Number, description: 'default 20' })
  async getAllUsers(@Query('offset') offset, @Query('limit') limit) {
    this.logger.log('Get all users');
    const users = await this.userService.findAll({
      skip: Number(offset) || 0,
      take: Number(limit) || 20,
    });
    const total = await this.userService.count();
    const results = users.map((user) => UsersResponseDto.fromUsersEntity(user));
    return {
      total,
      limit: Number(limit) || 20,
      offset: Number(offset) || 0,
      results,
    };
  }
}
