import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: ['http://localhost:3000', 'http://localhost:3001'],
    methods: ['GET'],
    credentials: true,
  });
  const config = new DocumentBuilder()
    .setTitle('USER-API')
    .setDescription('USER-API FOR ADMINISTRATORS')
    .setVersion('1.0')
    .addTag('USER-API')
    // .addApiKey(
    //   {
    //     type: 'apiKey', // this should be apiKey
    //     name: 'api-key', // this is the name of the key you expect in header
    //     in: 'header',
    //   },
    //   'access-key', // this is the name to show and used in swagger
    // )
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  await app.listen(3000);
}

bootstrap();
