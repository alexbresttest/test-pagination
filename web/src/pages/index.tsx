import Head from "next/head";
import { Inter } from "next/font/google";
import Table from "react-bootstrap/Table";
import { Alert, Container } from "react-bootstrap";
import Pagination from 'react-bootstrap/Pagination';
import { GetServerSideProps, GetServerSidePropsContext } from "next";
import { useEffect, useState } from "react";

const inter = Inter({ subsets: [ "latin" ] });

type TUserItem = {
  id: number
  firstname: string
  lastname: string
  email: string
  phone: string
  updatedAt: string
}

type TGetServerSideProps = {
  statusCode: number
  users: IUsers
}
type IUsers = {
  total: number,
  limit: number,
  offset: number,
  results: TUserItem[]
}

export const getServerSideProps = (async (ctx: GetServerSidePropsContext): Promise<{ props: TGetServerSideProps }> => {
  try {
    const res = await fetch("http://localhost:3000/users", { method: 'GET' });
    if (!res.ok) return { props: { statusCode: res.status, users: { total: 0, limit: 0, offset: 0, results: [] } } };
    return { props: { statusCode: 200, users: await res.json() } };
  } catch (e) {
    return { props: { statusCode: 500, users: { total: 0, limit: 0, offset: 0, results: [] } } };
  }
}) satisfies GetServerSideProps<TGetServerSideProps>;

export default function Home ({ statusCode, users }: TGetServerSideProps) {
  const [ active, setActive ] = useState(1);
  const [ firstVisibleNumber, setFirstVisibleNumber ] = useState(1);
  const [ usersArr, setUsersArr ] = useState(users as IUsers);

  useEffect(() => {
      const changeUsers = async () => {
        return await fetch("http://localhost:3000/users?offset=" + (active - 1) * 20, { method: 'GET' });
      };
      changeUsers().then(async (r) => setUsersArr(await r.json()));
    }
    , [ active ]);

  if (statusCode !== 200) {
    return <Alert variant={ 'danger' }>Ошибка { statusCode } при загрузке данных</Alert>;
  }

  function setActiveNumber (number: number) {
    setActive(number);
  }

  function changePagination (n: number) {
    if (active + n > 0 && active + n <= Math.ceil(users.total / 20)) {
      firstVisibleNumber + 9 < active + n ? setFirstVisibleNumber(firstVisibleNumber + 10) : null;
      firstVisibleNumber > active + n ? setFirstVisibleNumber(firstVisibleNumber - 10) : null;
      setActive(active + n);
    }
  }

  let items = [];
  for (let number = firstVisibleNumber; number <= firstVisibleNumber + 9 && number <= Math.ceil(users.total / 20); number++) {
    items.push(
      <Pagination.Item key={ number } active={ number === active }
                       onClick={ () => setActiveNumber(number) }>
        { number }
      </Pagination.Item>
    );
  }

  return (
    <>
      <Head>
        <title>Тестовое задание</title>
        <meta name="description" content="Тестовое задание"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="icon" href="/favicon.ico"/>
      </Head>

      <main className={ inter.className }>
        <Container>
          <h1 className={ 'mb-5' }>Пользователи</h1>

          <Table striped bordered hover>
            <thead>
            <tr>
              <th>ID</th>
              <th>Имя</th>
              <th>Фамилия</th>
              <th>Телефон</th>
              <th>Email</th>
              <th>Дата обновления</th>
            </tr>
            </thead>
            <tbody>
            {
              usersArr?.results?.map((user) => (
                <tr key={ user.id }>
                  <td>{ user.id }</td>
                  <td>{ user.firstname }</td>
                  <td>{ user.lastname }</td>
                  <td>{ user.phone }</td>
                  <td>{ user.email }</td>
                  <td>{ user.updatedAt }</td>
                </tr>
              ))
            }
            </tbody>
          </Table>

          <Pagination style={ { margin: "35px 0 35px 0", gap: "5px" } }>
            <Pagination.First onClick={ () => {
              setActive(1);
              setFirstVisibleNumber(1);
            } }/>
            <Pagination.Prev onClick={ () => changePagination(-1) }/>
            { items }
            <Pagination.Next onClick={ () => changePagination(1) }/>
            <Pagination.Last onClick={ () => {
              setActive(Math.ceil(users.total / 20));
              setFirstVisibleNumber(((Math.ceil(users.total / 200) - 1) * 10 + 1));
            } }/>
          </Pagination>

        </Container>
      </main>
    </>
  );
}
